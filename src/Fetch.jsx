import React, { useEffect } from 'react';
import Axios from 'axios';

function Fetch(props) {
  useEffect(() => {
    async function getInit() {
      props.dispatch.employeeDispatch({type: 'fetch'});
      try {
        const results = await Axios.get('http://dummy.restapiexample.com/api/v1/employees');
        props.dispatch.employeeDispatch({type: 'success', data: results.data.data});
      }
      catch(err) {
        props.dispatch.employeeDispatch({type: 'error', data: err.response.data});
      }
    }
    if (!props.state.employees.data) {
      getInit();
    }
  }, [props]);
  return (
    <div>
      {props.state.employees.status === 'fetching' && (
        <div>Fetching...</div>
      )}
      {props.state.employees.status === 'success' && (
        <ul>
          {props.state.employees.data.map(employee => <li key={employee.id}>{employee.employee_name}</li>)}
        </ul>
      )}
      {props.state.employees.status === 'error' && (
        <div>{props.state.employees.data.message}</div>
      )}
    </div>
  )
}

export default Fetch;
