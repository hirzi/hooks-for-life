import React, { useReducer } from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';
import { countingReducer, employeeReducer } from './reducers';
import Home from './Home';
import Counter from './Counter';
import CounterGlobal from './CounterGlobal';
import Fetch from './Fetch';
import Add from './Add';

const countingInitialState = 0;
const employeeInitialState = { data: 0 };

function App() {
  const [counting, countingDispatch] = useReducer(countingReducer, countingInitialState);
  const [employees, employeeDispatch] = useReducer(employeeReducer, employeeInitialState);

  return (
    <Router>
      <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/counter">Counter</Link>
            </li>
            <li>
              <Link to="/counter-global">Counter Global</Link>
            </li>
            <li>
              <Link to="/fetch">Fetch Employee</Link>
            </li>
            <li>
              <Link to="/add">Add Employee</Link>
            </li>
          </ul>
        </nav>
        <main>
          <Route exact path="/">
            <Home state={{counting, employees}}></Home>
          </Route>
          <Route exact path="/counter">
            <Counter></Counter>
          </Route>
          <Route exact path="/counter-global">
            <CounterGlobal state={{counting}} dispatch={{countingDispatch}}></CounterGlobal>
          </Route>
          <Route exact path="/fetch">
            <Fetch state={{employees}} dispatch={{employeeDispatch}}></Fetch>
          </Route>
          <Route exact path="/add">
            <Add state={{employees}} dispatch={{employeeDispatch}}></Add>
          </Route>
        </main>
    </Router>
  );
}

export default App;
