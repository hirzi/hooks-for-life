import React from 'react';

function Home(props) {
  return (
    <div>
      <p>the global conting is: {props.state.counting} times</p>
      <p>total employees: {props.state.employees.data.length || 0} person(s)</p>
    </div>
  )
}

export default Home;
