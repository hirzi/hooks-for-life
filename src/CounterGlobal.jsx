import React from 'react';

function CounterGlobal(props) {
  
  return (
    <div>
      <p>global conting : {props.state.counting} times</p>
      <button onClick={() => props.dispatch.countingDispatch({type: 'decrement'})}>-1</button>
      <button onClick={() => props.dispatch.countingDispatch({type: 'increment'})}>+1</button>
      <button onClick={() => props.dispatch.countingDispatch({type: 'set', data: 0})}>reset</button>
    </div>
  )
}

export default CounterGlobal;
