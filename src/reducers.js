const countingReducer = (state, action) => {
  switch(action.type) {
    case 'increment': return state + 1;
    case 'decrement': return state - 1;
    case 'set': return action.data;
    default: throw new Error('Unexpected action');
  }
}

const employeeReducer = (state, { type, data }) => {
  switch(type) {
    case 'fetch': return { status: 'fetching', data: [] };
    case 'success': return { status: 'success', data };
    case 'error': return { status: 'error', data };
    case 'sending': return { status: 'sending', data: state.data };
    case 'add': 
      state.data.push(data);
      return { status: 'add', data: state.data };
    case 'success_add': return { status: 'success', data: state.data };
    default: throw new Error('Unexpected action');
  }
}

export { countingReducer, employeeReducer };
