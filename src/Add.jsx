import React, { useState } from 'react';
import Axios from 'axios';

function Add(props) {
  const [name, setName] = useState('');
  const [salary, setSalary] = useState(0);
  const [age, setAge] = useState(0);
  return (
    <div>
      <form
        onSubmit={async (e) => {
          e.preventDefault();
          props.dispatch.employeeDispatch({type: 'sending'});
          try {
            const results = await Axios.post('http://dummy.restapiexample.com/api/v1/create', 
            { name, salary, age });
            props.dispatch.employeeDispatch({type: 'add', data: { 
              id: results.data.data.id, 
              employee_name: results.data.data.name, 
              employee_salary: results.data.data.salary,
              employee_age: results.data.data.age
            }});
            props.dispatch.employeeDispatch({type: 'success_add', data: results.data.data});
          }
          catch(err) {
            props.dispatch.employeeDispatch({type: 'error', data: err.response.data});
          }
        }}
        noValidate
      >
        <input
          type="text"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        <br/>
        <input
          type="number"
          min="0"
          value={salary}
          onChange={(e) => setSalary(e.target.value)}
        />
        <br/>
        <input
          type="number"
          min="0"
          value={age}
          onChange={(e) => setAge(e.target.value)}
        />
        <br/>
        <button type="submit">submit</button>
      </form>
    </div>
  )
}

export default Add;
